CREATE DATABASE Project;
USE Project;

CREATE TABLE ProjectInfo(
	ProjectId int,
	ProjectName Varchar(100),
	ProjectType Varchar(100),
	ProjectDetail Varchar(500)
);

ALTER TABLE ProjectInfo ADD ProjectData varchar(100);

ALTER TABLE ProjectInfo DROP COLUMN ProjectData;